import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventsModule } from './events/events.module';
import { MONGODB_URI } from './config/mongoose';
import { typeOrmConfig } from './config/typeorm';
import { GraphQLModule } from '@nestjs/graphql';
import { UserModule } from './user/user.module';
// import { UserService } from 'src/user/user.service';

@Module({
    imports: [
        MongooseModule.forRoot(MONGODB_URI, { useNewUrlParser: true }),
        TypeOrmModule.forRoot(typeOrmConfig),
        GraphQLModule.forRoot({
            typePaths: ['./**/*.graphql'],
            playground: true,
            // installSubscriptionHandlers: true,
            // resolverValidationOptions: {
            //     requireResolversForResolveType: false
            // },
        }),
        EventsModule,
        UserModule,
    ],
    // providers: [UserService],
})
export class AppModule {
}
