export const PG_USER = process.env.PG_USER || 'eska_support';
export const PG_PASSWORD = process.env.PG_PASSWORD || 'aaBm4Mpw7rkHHJY4';
export const PG_PORT = Number(process.env.PG_PORT) || 5902;
export const PG_DB = process.env.PG_DB || 'eska_support';
export const PG_HOST = process.env.PG_HOST || '0.0.0.0';
export const PG_URL = `postgres://${PG_USER}:${PG_PASSWORD}@${PG_HOST}:${PG_PORT}/${PG_DB}`;
