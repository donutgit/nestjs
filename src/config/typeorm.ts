import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { PG_DB, PG_HOST, PG_PASSWORD, PG_PORT, PG_USER } from './db';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

export const typeOrmConfig: TypeOrmModuleOptions = {
    type: 'postgres',
    host: PG_HOST,
    port: PG_PORT,
    username: PG_USER,
    password: PG_PASSWORD,
    database: PG_DB,
    entities: [__dirname + '/../**/*.entity.{js,ts}'],
    // namingStrategy: new SnakeNamingStrategy(),
    // synchronize: true,
};
console.log('types', typeOrmConfig);
