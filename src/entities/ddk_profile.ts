import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn,
    RelationId
} from 'typeorm';
import { User } from './/user';

@Entity('ddk_profile', { schema: 'public' })
export class DdkProfile {

    @PrimaryGeneratedColumn({
        type: 'integer',
        name: 'id'
    })
    id: number;


    @ManyToOne(() => User, (user: User) => user.ddkProfiles, { nullable: false, })
    @JoinColumn({ name: 'user_id' })
    user: User | null;


    @Column('character varying', {
        nullable: false,
        length: 255,
        name: 'full_name'
    })
    full_name: string;


    @Column('character varying', {
        nullable: false,
        length: 255,
        name: 'delegate_username'
    })
    delegate_username: string;


    @Column('character varying', {
        nullable: false,
        length: 255,
        name: 'ddk_address'
    })
    ddk_address: string;


    @Column('character varying', {
        nullable: false,
        length: 255,
        name: 'ddk_passphrase'
    })
    ddk_passphrase: string;


    @Column('character varying', {
        nullable: false,
        length: 255,
        name: 'country'
    })
    country: string;


    @Column('character varying', {
        nullable: true,
        length: 255,
        name: 'ssh_key'
    })
    ssh_key: string | null;


    @Column('character varying', {
        nullable: false,
        length: 255,
        name: 'ssh_login'
    })
    ssh_login: string;


    @Column('character varying', {
        nullable: false,
        length: 255,
        name: 'ssh_password'
    })
    ssh_password: string;


    @Column('inet', {
        nullable: false,
        name: 'ip_address'
    })
    ip_address: string;


    @Column('smallint', {
        nullable: false,
        name: 'port'
    })
    port: number;


    @Column('character varying', {
        nullable: false,
        length: 255,
        name: 'server_provider'
    })
    server_provider: string;


    @Column('character varying', {
        nullable: false,
        length: 255,
        name: 'os_version'
    })
    os_version: string;


    @Column('timestamp with time zone', {
        nullable: false,
        default: () => 'now()',
        name: 'created_at'
    })
    created_at: Date;


    @Column('timestamp with time zone', {
        nullable: false,
        default: () => 'now()',
        name: 'updated_at'
    })
    updated_at: Date;

}
