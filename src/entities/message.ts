import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn,
    RelationId
} from 'typeorm';
import { User } from './/user';
import { Ticket } from './/ticket';

@Entity('message', { schema: 'public' })
export class Message {

    @PrimaryGeneratedColumn({
        type: 'integer',
        name: 'id'
    })
    id: number;


    @ManyToOne(() => User, (user: User) => user.messages, { nullable: false, })
    @JoinColumn({ name: 'author_id' })
    author: User | null;


    @ManyToOne(() => User, (user: User) => user.messages2, { nullable: false, })
    @JoinColumn({ name: 'recipient' })
    recipient: User | null;


    @ManyToOne(() => Ticket, (ticket: Ticket) => ticket.messages, {})
    @JoinColumn({ name: 'ticket' })
    ticket: Ticket | null;


    @Column('text', {
        nullable: false,
        name: 'content'
    })
    content: string;


    @Column('boolean', {
        nullable: true,
        default: () => 'false',
        name: 'is_viewed'
    })
    is_viewed: boolean | null;


    @Column('boolean', {
        nullable: true,
        default: () => 'false',
        name: 'is_pinned'
    })
    is_pinned: boolean | null;


    @Column('timestamp with time zone', {
        nullable: false,
        default: () => 'now()',
        name: 'created_at'
    })
    created_at: Date;


    @Column('timestamp with time zone', {
        nullable: false,
        default: () => 'now()',
        name: 'updated_at'
    })
    updated_at: Date;

}
