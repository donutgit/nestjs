import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn,
    RelationId
} from 'typeorm';
import { User } from './/user';


@Entity('message_attachment', { schema: 'public' })
export class MessageAttachment {

    @PrimaryGeneratedColumn({
        type: 'integer',
        name: 'id'
    })
    id: number;


    @ManyToOne(() => User, (user: User) => user.messageAttachments, { nullable: false, })
    @JoinColumn({ name: 'message_id' })
    message: User | null;


    @Column('character varying', {
        nullable: false,
        length: 255,
        name: 'attachment'
    })
    attachment: string;

}
