import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn,
    RelationId
} from 'typeorm';
import { User } from './/user';
import { Message } from './/message';

@Entity('ticket', { schema: 'public' })
export class Ticket {

    @PrimaryGeneratedColumn({
        type: 'integer',
        name: 'id'
    })
    id: number;


    @Column('text', {
        nullable: false,
        name: 'name'
    })
    name: string;


    @Column('text', {
        nullable: true,
        name: 'description'
    })
    description: string | null;


    @ManyToOne(() => User, (user: User) => user.tickets2, { nullable: false, })
    @JoinColumn({ name: 'author_id' })
    author: User | null;


    @ManyToOne(() => User, (user: User) => user.tickets, { onDelete: 'CASCADE', })
    @JoinColumn({ name: 'assigned_to_id' })
    assignedTo: User | null;


    @Column('enum', {
        nullable: true,
        default: () => '\'OPEN\'',
        enum: ['OPEN', 'WIP', 'PENDING', 'RESOLVED', 'CLOSED'],
        name: 'status'
    })
    status: string | null;


    @Column('character varying', {
        nullable: true,
        length: 255,
        name: 'type'
    })
    type: string | null;


    @Column('enum', {
        nullable: true,
        default: () => '\'LOW\'',
        enum: ['LOW', 'MEDIUM', 'HIGH', 'IMMEDIATE'],
        name: 'priority'
    })
    priority: string | null;


    @Column('character varying', {
        nullable: true,
        length: 255,
        name: 'product'
    })
    product: string | null;


    @Column('boolean', {
        nullable: true,
        default: () => 'false',
        name: 'pinned'
    })
    pinned: boolean | null;


    @Column('timestamp with time zone', {
        nullable: false,
        default: () => 'now()',
        name: 'created_at'
    })
    created_at: Date;


    @Column('timestamp with time zone', {
        nullable: false,
        default: () => 'now()',
        name: 'updated_at'
    })
    updated_at: Date;


    @OneToMany(() => Message, (message: Message) => message.ticket)
    messages: Message[];

}
