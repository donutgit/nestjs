import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn,
    RelationId,
} from 'typeorm';
import { DdkProfile } from './/ddk_profile';
import { Message } from './/message';
import { MessageAttachment } from './/message_attachment';
import { Ticket } from './/ticket';

@Entity('user', { schema: 'public' })
@Index('user_email_key', ['email',], { unique: true })
@Index('user_username_key', ['username',], { unique: true })
export class User {

    @PrimaryGeneratedColumn({
        type: 'integer',
        name: 'id'
    })
    id: number;


    @Column('citext', {
        nullable: false,
        unique: true,
        name: 'username'
    })
    username: string;


    @Column('text', {
        nullable: true,
        name: 'avatar'
    })
    avatar: string | null;


    @Column('text', {
        nullable: true,
        name: 'about'
    })
    about: string | null;


    @Column('smallint', {
        nullable: true,
        default: () => '0',
        name: 'flag'
    })
    flag: number | null;


    @Column('citext', {
        nullable: false,
        unique: true,
        name: 'email'
    })
    email: string;


    @Column('text', {
        nullable: false,
        name: 'password'
    })
    password: string;


    @Column('enum', {
        nullable: false,
        default: () => '\'USER\'',
        enum: ['ADMIN', 'MANAGER', 'USER'],
        name: 'role'
    })
    role: string;


    @Column('boolean', {
        nullable: true,
        default: () => 'false',
        name: 'is_confirmed'
    })
    is_confirmed: boolean | null;


    @Column('boolean', {
        nullable: true,
        default: () => 'false',
        name: 'is_banned'
    })
    is_banned: boolean | null;


    @Column('boolean', {
        nullable: true,
        default: () => 'false',
        name: 'is_pinned'
    })
    is_pinned: boolean | null;


    @Column('timestamp with time zone', {
        nullable: false,
        default: () => 'now()',
        name: 'created_at'
    })
    created_at: Date;


    @Column('timestamp with time zone', {
        nullable: false,
        default: () => 'now()',
        name: 'updated_at'
    })
    updated_at: Date;

    @OneToMany(() => DdkProfile, (ddkProfile: DdkProfile) => ddkProfile.user)
    ddkProfiles: DdkProfile[];


    @OneToMany(() => Message, (message: Message) => message.author)
    messages: Message[];


    @OneToMany(() => Message, (message: Message) => message.recipient)
    messages2: Message[];


    @OneToMany(() => MessageAttachment, (messageAttachment: MessageAttachment) => messageAttachment.message)
    messageAttachments: MessageAttachment[];


    @OneToMany(() => Ticket, (ticket: Ticket) => ticket.assignedTo, { onDelete: 'CASCADE', })
    tickets: Ticket[];


    @OneToMany(() => Ticket, (ticket: Ticket) => ticket.author)
    tickets2: Ticket[];

}
