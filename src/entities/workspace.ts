import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn,
    RelationId
} from 'typeorm';

@Entity('workspace', { schema: 'public' })
export class Workspace {

    @PrimaryGeneratedColumn({
        type: 'integer',
        name: 'id'
    })
    id: number;


    @Column('character varying', {
        nullable: false,
        length: 255,
        name: 'name'
    })
    name: string;


    @Column('character varying', {
        nullable: false,
        length: 255,
        name: 'domain'
    })
    domain: string;


    @Column('character varying', {
        nullable: true,
        length: 255,
        name: 'url'
    })
    url: string | null;


    @Column('character varying', {
        nullable: true,
        length: 255,
        name: 'icon'
    })
    icon: string | null;


    @Column('character varying', {
        nullable: true,
        length: 255,
        name: 'avatar'
    })
    avatar: string | null;


    @Column('integer', {
        nullable: false,
        name: 'owner_id'
    })
    owner_id: number;

}
