import {
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
    WsResponse,
    OnGatewayConnection,
    OnGatewayDisconnect,
} from '@nestjs/websockets';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Client, Server } from 'socket.io';

@WebSocketGateway(4901, { transports: ['websocket'] })
export class EventsGateway implements OnGatewayConnection, OnGatewayDisconnect {
    @WebSocketServer()
    server: Server;
    users: number = 0;

    handleConnection(client: any, ...args: any[]) {
        this.users++;
        console.log('users: ', this.users);
        this.server.emit('users', this.users);
    }

    handleDisconnect(client: any) {
        this.users--;
        console.log('users: ', this.users);
        this.server.emit('users', this.users);
    }

    @SubscribeMessage('message')
    findAll(client: Client, data: any): Observable<WsResponse<number>> {
        console.log('message', data);
        return from([1, 2, 3]).pipe(map(item => ({ event: 'message', data: item })));
    }

    @SubscribeMessage('identity')
    async identity(client: Client, data: number): Promise<number> {
        return data;
    }
}
