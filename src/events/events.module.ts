import { Module, MiddlewareConsumer } from '@nestjs/common';
import { EventsGateway } from './events.gateway';

@Module({
    providers: [EventsGateway],
})
export class EventsModule {
}
