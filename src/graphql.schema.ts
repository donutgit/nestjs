import { UserEntity as UserEntityTypeorm } from './user/user.entity';

/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
export enum DdkProfilesOrderBy {
    NATURAL = 'NATURAL',
    ID_ASC = 'ID_ASC',
    ID_DESC = 'ID_DESC',
    PRIMARY_KEY_ASC = 'PRIMARY_KEY_ASC',
    PRIMARY_KEY_DESC = 'PRIMARY_KEY_DESC'
}

export enum MessageAttachmentsOrderBy {
    NATURAL = 'NATURAL',
    ID_ASC = 'ID_ASC',
    ID_DESC = 'ID_DESC',
    PRIMARY_KEY_ASC = 'PRIMARY_KEY_ASC',
    PRIMARY_KEY_DESC = 'PRIMARY_KEY_DESC'
}

export enum MessagesOrderBy {
    NATURAL = 'NATURAL',
    ID_ASC = 'ID_ASC',
    ID_DESC = 'ID_DESC',
    PRIMARY_KEY_ASC = 'PRIMARY_KEY_ASC',
    PRIMARY_KEY_DESC = 'PRIMARY_KEY_DESC'
}

export enum TicketPriority {
    LOW = 'LOW',
    MEDIUM = 'MEDIUM',
    HIGH = 'HIGH',
    IMMEDIATE = 'IMMEDIATE'
}

export enum TicketsOrderBy {
    NATURAL = 'NATURAL',
    ID_ASC = 'ID_ASC',
    ID_DESC = 'ID_DESC',
    PRIMARY_KEY_ASC = 'PRIMARY_KEY_ASC',
    PRIMARY_KEY_DESC = 'PRIMARY_KEY_DESC'
}

export enum TicketStatus {
    OPEN = 'OPEN',
    WIP = 'WIP',
    PENDING = 'PENDING',
    RESOLVED = 'RESOLVED',
    CLOSED = 'CLOSED'
}

export enum UserEntitiesOrderBy {
    NATURAL = 'NATURAL',
    ID_ASC = 'ID_ASC',
    ID_DESC = 'ID_DESC',
    PRIMARY_KEY_ASC = 'PRIMARY_KEY_ASC',
    PRIMARY_KEY_DESC = 'PRIMARY_KEY_DESC'
}

export enum UserRoles {
    ADMIN = 'ADMIN',
    MANAGER = 'MANAGER',
    USER = 'USER'
}

export enum UsersOrderBy {
    NATURAL = 'NATURAL',
    ID_ASC = 'ID_ASC',
    ID_DESC = 'ID_DESC',
    USERNAME_ASC = 'USERNAME_ASC',
    USERNAME_DESC = 'USERNAME_DESC',
    EMAIL_ASC = 'EMAIL_ASC',
    EMAIL_DESC = 'EMAIL_DESC',
    PRIMARY_KEY_ASC = 'PRIMARY_KEY_ASC',
    PRIMARY_KEY_DESC = 'PRIMARY_KEY_DESC'
}

export enum WorkspacesOrderBy {
    NATURAL = 'NATURAL',
    ID_ASC = 'ID_ASC',
    ID_DESC = 'ID_DESC',
    PRIMARY_KEY_ASC = 'PRIMARY_KEY_ASC',
    PRIMARY_KEY_DESC = 'PRIMARY_KEY_DESC'
}

export class CreateDdkProfileInput {
    clientMutationId?: string;
    ddkProfile: DdkProfileInput;
}

export class CreateMessageAttachmentInput {
    clientMutationId?: string;
    messageAttachment: MessageAttachmentInput;
}

export class CreateMessageInput {
    clientMutationId?: string;
    message: MessageInput;
}

export class CreateTicketInput {
    clientMutationId?: string;
    ticket: TicketInput;
}

export class CreateUserEntityInput {
    clientMutationId?: string;
    userEntity: UserEntityInput;
}

export class CreateUserInput {
    clientMutationId?: string;
    user: UserInput;
}

export class CreateWorkspaceInput {
    clientMutationId?: string;
    workspace: WorkspaceInput;
}

export class DdkProfileCondition {
    id?: number;
}

export class DdkProfileInput {
    id?: number;
    userId: number;
    fullName: string;
    delegateUsername: string;
    ddkAddress: string;
    ddkPassphrase: string;
    country: string;
    sshKey?: string;
    sshLogin: string;
    sshPassword: string;
    ipAddress: InternetAddress;
    port: number;
    serverProvider: string;
    osVersion: string;
    createdAt?: Datetime;
    updatedAt?: Datetime;
}

export class DdkProfilePatch {
    id?: number;
    userId?: number;
    fullName?: string;
    delegateUsername?: string;
    ddkAddress?: string;
    ddkPassphrase?: string;
    country?: string;
    sshKey?: string;
    sshLogin?: string;
    sshPassword?: string;
    ipAddress?: InternetAddress;
    port?: number;
    serverProvider?: string;
    osVersion?: string;
    createdAt?: Datetime;
    updatedAt?: Datetime;
}

export class DeleteDdkProfileByIdInput {
    clientMutationId?: string;
    id: number;
}

export class DeleteDdkProfileInput {
    clientMutationId?: string;
    nodeId: string;
}

export class DeleteMessageAttachmentByIdInput {
    clientMutationId?: string;
    id: number;
}

export class DeleteMessageAttachmentInput {
    clientMutationId?: string;
    nodeId: string;
}

export class DeleteMessageByIdInput {
    clientMutationId?: string;
    id: number;
}

export class DeleteMessageInput {
    clientMutationId?: string;
    nodeId: string;
}

export class DeleteTicketByIdInput {
    clientMutationId?: string;
    id: number;
}

export class DeleteTicketInput {
    clientMutationId?: string;
    nodeId: string;
}

export class DeleteUserByEmailInput {
    clientMutationId?: string;
    email: string;
}

export class DeleteUserByIdInput {
    clientMutationId?: string;
    id: number;
}

export class DeleteUserByUsernameInput {
    clientMutationId?: string;
    username: string;
}

export class DeleteUserEntityByIdInput {
    clientMutationId?: string;
    id: number;
}

export class DeleteUserEntityInput {
    clientMutationId?: string;
    nodeId: string;
}

export class DeleteUserInput {
    clientMutationId?: string;
    nodeId: string;
}

export class DeleteWorkspaceByIdInput {
    clientMutationId?: string;
    id: number;
}

export class DeleteWorkspaceInput {
    clientMutationId?: string;
    nodeId: string;
}

export class MessageAttachmentCondition {
    id?: number;
}

export class MessageAttachmentInput {
    id?: number;
    messageId: number;
    attachment: string;
}

export class MessageAttachmentPatch {
    id?: number;
    messageId?: number;
    attachment?: string;
}

export class MessageCondition {
    id?: number;
}

export class MessageInput {
    id?: number;
    authorId: number;
    recipient: number;
    ticket?: number;
    content: string;
    isViewed?: boolean;
    isPinned?: boolean;
    createdAt?: Datetime;
    updatedAt?: Datetime;
}

export class MessagePatch {
    id?: number;
    authorId?: number;
    recipient?: number;
    ticket?: number;
    content?: string;
    isViewed?: boolean;
    isPinned?: boolean;
    createdAt?: Datetime;
    updatedAt?: Datetime;
}

export class TicketCondition {
    id?: number;
}

export class TicketInput {
    id?: number;
    name: string;
    description?: string;
    authorId: number;
    assignedToId?: number;
    status?: TicketStatus;
    type?: string;
    priority?: TicketPriority;
    product?: string;
    pinned?: boolean;
    createdAt?: Datetime;
    updatedAt?: Datetime;
}

export class TicketPatch {
    id?: number;
    name?: string;
    description?: string;
    authorId?: number;
    assignedToId?: number;
    status?: TicketStatus;
    type?: string;
    priority?: TicketPriority;
    product?: string;
    pinned?: boolean;
    createdAt?: Datetime;
    updatedAt?: Datetime;
}

export class UpdateDdkProfileByIdInput {
    clientMutationId?: string;
    ddkProfilePatch: DdkProfilePatch;
    id: number;
}

export class UpdateDdkProfileInput {
    clientMutationId?: string;
    nodeId: string;
    ddkProfilePatch: DdkProfilePatch;
}

export class UpdateMessageAttachmentByIdInput {
    clientMutationId?: string;
    messageAttachmentPatch: MessageAttachmentPatch;
    id: number;
}

export class UpdateMessageAttachmentInput {
    clientMutationId?: string;
    nodeId: string;
    messageAttachmentPatch: MessageAttachmentPatch;
}

export class UpdateMessageByIdInput {
    clientMutationId?: string;
    messagePatch: MessagePatch;
    id: number;
}

export class UpdateMessageInput {
    clientMutationId?: string;
    nodeId: string;
    messagePatch: MessagePatch;
}

export class UpdateTicketByIdInput {
    clientMutationId?: string;
    ticketPatch: TicketPatch;
    id: number;
}

export class UpdateTicketInput {
    clientMutationId?: string;
    nodeId: string;
    ticketPatch: TicketPatch;
}

export class UpdateUserByEmailInput {
    clientMutationId?: string;
    userPatch: UserPatch;
    email: string;
}

export class UpdateUserByIdInput {
    clientMutationId?: string;
    userPatch: UserPatch;
    id: number;
}

export class UpdateUserByUsernameInput {
    clientMutationId?: string;
    userPatch: UserPatch;
    username: string;
}

export class UpdateUserEntityByIdInput {
    clientMutationId?: string;
    userEntityPatch: UserEntityPatch;
    id: number;
}

export class UpdateUserEntityInput {
    clientMutationId?: string;
    nodeId: string;
    userEntityPatch: UserEntityPatch;
}

export class UpdateUserInput {
    clientMutationId?: string;
    nodeId: string;
    userPatch: UserPatch;
}

export class UpdateWorkspaceByIdInput {
    clientMutationId?: string;
    workspacePatch: WorkspacePatch;
    id: number;
}

export class UpdateWorkspaceInput {
    clientMutationId?: string;
    nodeId: string;
    workspacePatch: WorkspacePatch;
}

export class UserCondition {
    id?: number;
    username?: string;
    email?: string;
}

export class UserEntityCondition {
    id?: number;
}

export class UserEntityInput {
    id?: number;
    name: string;
}

export class UserEntityPatch {
    id?: number;
    name?: string;
}

export class UserInput {
    id?: number;
    username: string;
    avatar?: string;
    about?: string;
    flag?: number;
    email: string;
    password: string;
    role?: UserRoles;
    isConfirmed?: boolean;
    isBanned?: boolean;
    isPinned?: boolean;
    createdAt?: Datetime;
    updatedAt?: Datetime;
}

export class UserPatch {
    id?: number;
    username?: string;
    avatar?: string;
    about?: string;
    flag?: number;
    email?: string;
    password?: string;
    role?: UserRoles;
    isConfirmed?: boolean;
    isBanned?: boolean;
    isPinned?: boolean;
    createdAt?: Datetime;
    updatedAt?: Datetime;
}

export class WorkspaceCondition {
    id?: number;
}

export class WorkspaceInput {
    id?: number;
    name: string;
    domain: string;
    url?: string;
    icon?: string;
    avatar?: string;
    ownerId: number;
}

export class WorkspacePatch {
    id?: number;
    name?: string;
    domain?: string;
    url?: string;
    icon?: string;
    avatar?: string;
    ownerId?: number;
}

export interface Node {
    nodeId: string;
}

export class CreateDdkProfilePayload {
    clientMutationId?: string;
    ddkProfile?: DdkProfile;
    query?: Query;
    ddkProfileEdge?: DdkProfilesEdge;
}

export class CreateMessageAttachmentPayload {
    clientMutationId?: string;
    messageAttachment?: MessageAttachment;
    query?: Query;
    messageAttachmentEdge?: MessageAttachmentsEdge;
}

export class CreateMessagePayload {
    clientMutationId?: string;
    message?: Message;
    query?: Query;
    messageEdge?: MessagesEdge;
}

export class CreateTicketPayload {
    clientMutationId?: string;
    ticket?: Ticket;
    query?: Query;
    ticketEdge?: TicketsEdge;
}

export class CreateUserEntityPayload {
    clientMutationId?: string;
    userEntity?: UserEntity;
    query?: Query;
    userEntityEdge?: UserEntitiesEdge;
}

export class CreateUserPayload {
    clientMutationId?: string;
    user?: User;
    query?: Query;
    userEdge?: UsersEdge;
}

export class CreateWorkspacePayload {
    clientMutationId?: string;
    workspace?: Workspace;
    query?: Query;
    workspaceEdge?: WorkspacesEdge;
}

export class DdkProfile implements Node {
    nodeId: string;
    id: number;
    userId: number;
    fullName: string;
    delegateUsername: string;
    ddkAddress: string;
    ddkPassphrase: string;
    country: string;
    sshKey?: string;
    sshLogin: string;
    sshPassword: string;
    ipAddress: InternetAddress;
    port: number;
    serverProvider: string;
    osVersion: string;
    createdAt: Datetime;
    updatedAt: Datetime;
}

export class DdkProfilesConnection {
    nodes: DdkProfile[];
    edges: DdkProfilesEdge[];
    pageInfo: PageInfo;
    totalCount: number;
}

export class DdkProfilesEdge {
    cursor?: Cursor;
    node?: DdkProfile;
}

export class DeleteDdkProfilePayload {
    clientMutationId?: string;
    ddkProfile?: DdkProfile;
    deletedDdkProfileId?: string;
    query?: Query;
    ddkProfileEdge?: DdkProfilesEdge;
}

export class DeleteMessageAttachmentPayload {
    clientMutationId?: string;
    messageAttachment?: MessageAttachment;
    deletedMessageAttachmentId?: string;
    query?: Query;
    messageAttachmentEdge?: MessageAttachmentsEdge;
}

export class DeleteMessagePayload {
    clientMutationId?: string;
    message?: Message;
    deletedMessageId?: string;
    query?: Query;
    messageEdge?: MessagesEdge;
}

export class DeleteTicketPayload {
    clientMutationId?: string;
    ticket?: Ticket;
    deletedTicketId?: string;
    query?: Query;
    ticketEdge?: TicketsEdge;
}

export class DeleteUserEntityPayload {
    clientMutationId?: string;
    userEntity?: UserEntity;
    deletedUserEntityId?: string;
    query?: Query;
    userEntityEdge?: UserEntitiesEdge;
}

export class DeleteUserPayload {
    clientMutationId?: string;
    user?: User;
    deletedUserId?: string;
    query?: Query;
    userEdge?: UsersEdge;
}

export class DeleteWorkspacePayload {
    clientMutationId?: string;
    workspace?: Workspace;
    deletedWorkspaceId?: string;
    query?: Query;
    workspaceEdge?: WorkspacesEdge;
}

export class Message implements Node {
    nodeId: string;
    id: number;
    authorId: number;
    recipient: number;
    ticket?: number;
    content: string;
    isViewed?: boolean;
    isPinned?: boolean;
    createdAt: Datetime;
    updatedAt: Datetime;
}

export class MessageAttachment implements Node {
    nodeId: string;
    id: number;
    messageId: number;
    attachment: string;
}

export class MessageAttachmentsConnection {
    nodes: MessageAttachment[];
    edges: MessageAttachmentsEdge[];
    pageInfo: PageInfo;
    totalCount: number;
}

export class MessageAttachmentsEdge {
    cursor?: Cursor;
    node?: MessageAttachment;
}

export class MessagesConnection {
    nodes: Message[];
    edges: MessagesEdge[];
    pageInfo: PageInfo;
    totalCount: number;
}

export class MessagesEdge {
    cursor?: Cursor;
    node?: Message;
}

export abstract class IMutation {
    abstract createDdkProfile(input: CreateDdkProfileInput): CreateDdkProfilePayload | Promise<CreateDdkProfilePayload>;

    abstract createMessage(input: CreateMessageInput): CreateMessagePayload | Promise<CreateMessagePayload>;

    abstract createMessageAttachment(input: CreateMessageAttachmentInput): CreateMessageAttachmentPayload | Promise<CreateMessageAttachmentPayload>;

    abstract createTicket(input: CreateTicketInput): CreateTicketPayload | Promise<CreateTicketPayload>;

    abstract createUser(input: CreateUserInput): CreateUserPayload | Promise<CreateUserPayload>;

    abstract createUserEntity(input: CreateUserEntityInput): CreateUserEntityPayload | Promise<CreateUserEntityPayload>;

    abstract createWorkspace(input: CreateWorkspaceInput): CreateWorkspacePayload | Promise<CreateWorkspacePayload>;

    abstract updateDdkProfile(input: UpdateDdkProfileInput): UpdateDdkProfilePayload | Promise<UpdateDdkProfilePayload>;

    abstract updateDdkProfileById(input: UpdateDdkProfileByIdInput): UpdateDdkProfilePayload | Promise<UpdateDdkProfilePayload>;

    abstract updateMessage(input: UpdateMessageInput): UpdateMessagePayload | Promise<UpdateMessagePayload>;

    abstract updateMessageById(input: UpdateMessageByIdInput): UpdateMessagePayload | Promise<UpdateMessagePayload>;

    abstract updateMessageAttachment(input: UpdateMessageAttachmentInput): UpdateMessageAttachmentPayload | Promise<UpdateMessageAttachmentPayload>;

    abstract updateMessageAttachmentById(input: UpdateMessageAttachmentByIdInput): UpdateMessageAttachmentPayload | Promise<UpdateMessageAttachmentPayload>;

    abstract updateTicket(input: UpdateTicketInput): UpdateTicketPayload | Promise<UpdateTicketPayload>;

    abstract updateTicketById(input: UpdateTicketByIdInput): UpdateTicketPayload | Promise<UpdateTicketPayload>;

    abstract updateUser(input: UpdateUserInput): UpdateUserPayload | Promise<UpdateUserPayload>;

    abstract updateUserById(input: UpdateUserByIdInput): UpdateUserPayload | Promise<UpdateUserPayload>;

    abstract updateUserByUsername(input: UpdateUserByUsernameInput): UpdateUserPayload | Promise<UpdateUserPayload>;

    abstract updateUserByEmail(input: UpdateUserByEmailInput): UpdateUserPayload | Promise<UpdateUserPayload>;

    abstract updateUserEntity(input: UpdateUserEntityInput): UpdateUserEntityPayload | Promise<UpdateUserEntityPayload>;

    abstract updateUserEntityById(input: UpdateUserEntityByIdInput): UpdateUserEntityPayload | Promise<UpdateUserEntityPayload>;

    abstract updateWorkspace(input: UpdateWorkspaceInput): UpdateWorkspacePayload | Promise<UpdateWorkspacePayload>;

    abstract updateWorkspaceById(input: UpdateWorkspaceByIdInput): UpdateWorkspacePayload | Promise<UpdateWorkspacePayload>;

    abstract deleteDdkProfile(input: DeleteDdkProfileInput): DeleteDdkProfilePayload | Promise<DeleteDdkProfilePayload>;

    abstract deleteDdkProfileById(input: DeleteDdkProfileByIdInput): DeleteDdkProfilePayload | Promise<DeleteDdkProfilePayload>;

    abstract deleteMessage(input: DeleteMessageInput): DeleteMessagePayload | Promise<DeleteMessagePayload>;

    abstract deleteMessageById(input: DeleteMessageByIdInput): DeleteMessagePayload | Promise<DeleteMessagePayload>;

    abstract deleteMessageAttachment(input: DeleteMessageAttachmentInput): DeleteMessageAttachmentPayload | Promise<DeleteMessageAttachmentPayload>;

    abstract deleteMessageAttachmentById(input: DeleteMessageAttachmentByIdInput): DeleteMessageAttachmentPayload | Promise<DeleteMessageAttachmentPayload>;

    abstract deleteTicket(input: DeleteTicketInput): DeleteTicketPayload | Promise<DeleteTicketPayload>;

    abstract deleteTicketById(input: DeleteTicketByIdInput): DeleteTicketPayload | Promise<DeleteTicketPayload>;

    abstract deleteUser(input: DeleteUserInput): DeleteUserPayload | Promise<DeleteUserPayload>;

    abstract deleteUserById(input: DeleteUserByIdInput): DeleteUserPayload | Promise<DeleteUserPayload>;

    abstract deleteUserByUsername(input: DeleteUserByUsernameInput): DeleteUserPayload | Promise<DeleteUserPayload>;

    abstract deleteUserByEmail(input: DeleteUserByEmailInput): DeleteUserPayload | Promise<DeleteUserPayload>;

    abstract deleteUserEntity(input: DeleteUserEntityInput): DeleteUserEntityPayload | Promise<DeleteUserEntityPayload>;

    abstract deleteUserEntityById(input: DeleteUserEntityByIdInput): DeleteUserEntityPayload | Promise<DeleteUserEntityPayload>;

    abstract deleteWorkspace(input: DeleteWorkspaceInput): DeleteWorkspacePayload | Promise<DeleteWorkspacePayload>;

    abstract deleteWorkspaceById(input: DeleteWorkspaceByIdInput): DeleteWorkspacePayload | Promise<DeleteWorkspacePayload>;
}

export class PageInfo {
    hasNextPage: boolean;
    hasPreviousPage: boolean;
    startCursor?: Cursor;
    endCursor?: Cursor;
}

export abstract class IQuery implements Node {
    abstract query(): Query | Promise<Query>;

    // abstract nodeId(): string | Promise<string>;
    abstract nodeId: string;

    abstract node(nodeId: string): Node | Promise<Node>;

    abstract allDdkProfiles(first?: number, last?: number, offset?: number, before?: Cursor, after?: Cursor, orderBy?: DdkProfilesOrderBy[], condition?: DdkProfileCondition): DdkProfilesConnection | Promise<DdkProfilesConnection>;

    abstract allMessages(first?: number, last?: number, offset?: number, before?: Cursor, after?: Cursor, orderBy?: MessagesOrderBy[], condition?: MessageCondition): MessagesConnection | Promise<MessagesConnection>;

    abstract allMessageAttachments(first?: number, last?: number, offset?: number, before?: Cursor, after?: Cursor, orderBy?: MessageAttachmentsOrderBy[], condition?: MessageAttachmentCondition): MessageAttachmentsConnection | Promise<MessageAttachmentsConnection>;

    abstract allTickets(first?: number, last?: number, offset?: number, before?: Cursor, after?: Cursor, orderBy?: TicketsOrderBy[], condition?: TicketCondition): TicketsConnection | Promise<TicketsConnection>;

    abstract allUsers(first?: number, last?: number, offset?: number, before?: Cursor, after?: Cursor, orderBy?: UsersOrderBy[], condition?: UserCondition): UsersConnection | Promise<UsersConnection>;

    abstract allUserEntities(first?: number, last?: number, offset?: number, before?: Cursor, after?: Cursor, orderBy?: UserEntitiesOrderBy[], condition?: UserEntityCondition): UserEntitiesConnection | Promise<UserEntitiesConnection>;

    abstract allWorkspaces(first?: number, last?: number, offset?: number, before?: Cursor, after?: Cursor, orderBy?: WorkspacesOrderBy[], condition?: WorkspaceCondition): WorkspacesConnection | Promise<WorkspacesConnection>;

    abstract ddkProfileById(id: number): DdkProfile | Promise<DdkProfile>;

    abstract messageById(id: number): Message | Promise<Message>;

    abstract messageAttachmentById(id: number): MessageAttachment | Promise<MessageAttachment>;

    abstract ticketById(id: number): Ticket | Promise<Ticket>;

    abstract userById(id: number): User | Promise<User>;

    abstract userByUsername(username: string): User | Promise<User>;

    abstract userByEmail(email: string): User | Promise<User>;

    abstract userEntityById(id: number): UserEntity | Promise<UserEntity>;

    abstract workspaceById(id: number): Workspace | Promise<Workspace>;

    abstract ddkProfile(nodeId: string): DdkProfile | Promise<DdkProfile>;

    abstract message(nodeId: string): Message | Promise<Message>;

    abstract messageAttachment(nodeId: string): MessageAttachment | Promise<MessageAttachment>;

    abstract ticket(nodeId: string): Ticket | Promise<Ticket>;

    abstract user(nodeId: string): User | Promise<User>;

    abstract userEntity(nodeId: string): UserEntity | Promise<UserEntity>;

    abstract workspace(nodeId: string): Workspace | Promise<Workspace>;
}

export class Ticket implements Node {
    nodeId: string;
    id: number;
    name: string;
    description?: string;
    authorId: number;
    assignedToId?: number;
    status?: TicketStatus;
    type?: string;
    priority?: TicketPriority;
    product?: string;
    pinned?: boolean;
    createdAt: Datetime;
    updatedAt: Datetime;
}

export class TicketsConnection {
    nodes: Ticket[];
    edges: TicketsEdge[];
    pageInfo: PageInfo;
    totalCount: number;
}

export class TicketsEdge {
    cursor?: Cursor;
    node?: Ticket;
}

export class UpdateDdkProfilePayload {
    clientMutationId?: string;
    ddkProfile?: DdkProfile;
    query?: Query;
    ddkProfileEdge?: DdkProfilesEdge;
}

export class UpdateMessageAttachmentPayload {
    clientMutationId?: string;
    messageAttachment?: MessageAttachment;
    query?: Query;
    messageAttachmentEdge?: MessageAttachmentsEdge;
}

export class UpdateMessagePayload {
    clientMutationId?: string;
    message?: Message;
    query?: Query;
    messageEdge?: MessagesEdge;
}

export class UpdateTicketPayload {
    clientMutationId?: string;
    ticket?: Ticket;
    query?: Query;
    ticketEdge?: TicketsEdge;
}

export class UpdateUserEntityPayload {
    clientMutationId?: string;
    userEntity?: UserEntity;
    query?: Query;
    userEntityEdge?: UserEntitiesEdge;
}

export class UpdateUserPayload {
    clientMutationId?: string;
    user?: User;
    query?: Query;
    userEdge?: UsersEdge;
}

export class UpdateWorkspacePayload {
    clientMutationId?: string;
    workspace?: Workspace;
    query?: Query;
    workspaceEdge?: WorkspacesEdge;
}

export class User implements Node {
    nodeId: string;
    id: number;
    username: string;
    avatar?: string;
    about?: string;
    flag?: number;
    email: string;
    password: string;
    role: UserRoles;
    isConfirmed?: boolean;
    isBanned?: boolean;
    isPinned?: boolean;
    createdAt: Datetime;
    updatedAt: Datetime;
}

export class UserEntitiesConnection {
    nodes: UserEntity[];
    edges: UserEntitiesEdge[];
    pageInfo: PageInfo;
    totalCount: number;
}

export class UserEntitiesEdge {
    cursor?: Cursor;
    node?: UserEntity;
}

export class UserEntity implements Node {
    nodeId: string;
    id: number;
    name: string;
}

export class UsersConnection {
    nodes: UserEntityTypeorm[];
    edges: UsersEdge[];
    pageInfo: PageInfo;
    totalCount: number;
}

export class UsersEdge {
    cursor?: Cursor;
    node?: User;
}

export class Workspace implements Node {
    nodeId: string;
    id: number;
    name: string;
    domain: string;
    url?: string;
    icon?: string;
    avatar?: string;
    ownerId: number;
}

export class WorkspacesConnection {
    nodes: Workspace[];
    edges: WorkspacesEdge[];
    pageInfo: PageInfo;
    totalCount: number;
}

export class WorkspacesEdge {
    cursor?: Cursor;
    node?: Workspace;
}

export type Cursor = any;
export type Datetime = any;
export type InternetAddress = any;
export type Query = any;
