import { Query, Resolver, Args, ResolveProperty, Parent, Mutation } from '@nestjs/graphql';
import { UserService } from './user.service';
import { Logger } from '@nestjs/common';
import {
    User,
    IQuery,
    Cursor,
    UsersOrderBy,
    UserCondition,
    UsersConnection,
    UpdateUserByIdInput
} from '../graphql.schema';
import { UserEntity } from 'src/user/user.entity';

export interface IAllUsersQuery {
    first?: number;
    last?: number;
    offset?: number;
    before?: Cursor;
    after?: Cursor;
    orderBy?: UsersOrderBy[];
    condition?: UserCondition;
}

@Resolver('User')
export class UserResolver {
    constructor(
        private readonly userService: UserService,
    ) {
    }

    @Query()
    async userById(@Args('id') id: number): Promise<UserEntity> {
        const res = await this.userService.userById(id);
        Logger.debug(`🚀userById: ${JSON.stringify(id)}`, 'Query');
        console.log('res: ', res);
        return {
            ...res,
        };
    }

    @Query()
    async userByEmail(@Args('email') email: string): Promise<UserEntity> {
        const res = await this.userService.userByEmail(email);
        Logger.debug(`🚀userById: ${JSON.stringify(email)}`, 'Query');
        console.log('res: ', res);
        return res;
    }

    @Query()
    async allUsers(@Args() query: IAllUsersQuery): Promise<UsersConnection> {
        const [nodes, totalCount] = await this.userService.allUsers(query);
        Logger.debug(`🚀userById query: ${JSON.stringify(query)}`, 'Query');
        return {
            nodes,
            totalCount,
            pageInfo: null,
            edges: null,
        };
    }

    @Mutation()
    async updateUserById(@Args('input') input: UpdateUserByIdInput): Promise<any> {
        const res = await this.userService.updateUserById(input);
        return res;
    }

}
