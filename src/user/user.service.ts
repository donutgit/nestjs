import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from './user.entity';
import { IAllUsersQuery } from './user.resolver';
import { UpdateUserByIdInput } from 'src/graphql.schema';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
    ) {
    }

    async userById(id: number): Promise<UserEntity> {
        return await this.userRepository.findOne({ where: { id } });

    }

    async userByEmail(email: string): Promise<UserEntity> {
        return await this.userRepository.findOne({ where: { email } });
    }

    async allUsers(query: IAllUsersQuery): Promise<[UserEntity[], number]> {
        return await this.userRepository.findAndCount();
    }

    async updateUserById(input: UpdateUserByIdInput): Promise<UserEntity> {
        console.log('UpdateUserByIdInput', input);
        const res = await this.userById(input.id);
        console.log('find one', res);
        const update = {
            ...res,
            ...input.userPatch,
        };
        const res2 = await this.userRepository.save({
            ...res,
            ...input.userPatch
        });
        console.log('update result', res2);
        return res2;
        // return await this.userRepository.update(query.id, { username: 'updated_username' })
    }

}
